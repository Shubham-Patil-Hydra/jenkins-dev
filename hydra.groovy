pipeline {
    agent any
    stages{
        stage ('pull'){
            steps{
                git 'https://gitlab.com/Shubham-Patil-Hydra/studentapp-ui1.git'
                echo 'Pull completed'
            }
        }
        stage ('build'){
            steps{
                sh '/opt/apache-maven-3.9.5/bin/mvn clean package'
                echo 'Build completed'
            }
        }
        stage ('Deploy'){
            steps{
                echo 'Deploy completed'
            }
        }
    }
}